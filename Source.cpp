#include "Source.h"
#include <iostream>

using namespace std;

class Source::CreateEntityEvent : public Event {
private:
	Source* _source;

public:
	CreateEntityEvent(Source* source) {
		_source = source;
	}
	void Execute() {
		_source->CreateEntity();
	}
};
void Source::CreateEntity() {
	if ((_numGen == 1) || (_numGen > 0))
	{
		if (_numGen > 0) {
			_numGen--;
		}
		Entity* en = new Entity(GetCurrentSimTime());
		_queue->ScheduleArrivalIn(0.0, en);

		ScheduleEventIn(_interarrivalTime->GetRV(), new CreateEntityEvent(this));
	}
}
Source::Source(string name, Queue* queue, Distribution* interarrivalTime) {
	_queue = queue;
	_interarrivalTime = interarrivalTime;
	ScheduleEventAt(0.0, new CreateEntityEvent(this));

	_name = name;
	_numGen = -1;
}
Source::Source(string name, Queue* queue, Distribution* interarrivalTime, int numGen) {
	_queue = queue;
	_interarrivalTime = interarrivalTime;
	ScheduleEventAt(0.0, new CreateEntityEvent(this));
	_name = name;
	_numGen = numGen;
}