#include "Sink.h"
#include "SimObj.h"
Sink::Sink(string name)
{
	_name = name;
}

class Sink::Stats {

public:
	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	Stats() {
		_eventsCount = 0;
		_totalFlowtime = 0;
		_totalDelaytime = 0;
		_maxFlowtime = 0;
		_maxDelaytime = 0;
		_sumOfServiceTimes = 0;
		_completionTime = 0;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	int GetEventsCount()
	{
		return _eventsCount;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	Time GetMaxFlowtime()
	{
		return _maxFlowtime;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	Time GetMaxDelaytime()
	{
		return _maxDelaytime;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	Time GetAvgFlowtime()
	{
		return _totalFlowtime / _eventsCount;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	Time  GetAvgDelaytime()
	{
		return _totalDelaytime / _eventsCount;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	Time  GetServerUtilization()
	{
		//For a single server system, assuming current time is the total time of simulation, we use it to calculate server utilization
		return _sumOfServiceTimes / _completionTime;
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	void AddStats(Time delayTime, Time flowTime, Time serviceTime,Time completionTime) {
		_eventsCount++; //Increase event count
		_sumOfServiceTimes += serviceTime; // add entity service time

		_totalDelaytime += delayTime; // add entity delay time
		_maxDelaytime = delayTime > _maxDelaytime ? delayTime : _maxDelaytime; // Set max delaytime

		_totalFlowtime += flowTime; // add entity flow time
		_maxFlowtime = flowTime > _maxFlowtime ? flowTime : _maxFlowtime; // Set max flowtime
		_completionTime = completionTime; //last last completion time
	}

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	void GetStats()
	{
		cout << "Number of Events " << GetEventsCount() << endl;
		cout << "Max Delay time " << GetMaxDelaytime() << endl;
		cout << "Max Flow time " << GetMaxFlowtime() << endl;
		cout << "Avg Delay time " << GetAvgDelaytime() << endl;
		cout << "Avg Flow time " << GetAvgFlowtime() << endl;
		cout << "Avg Delay time " << GetAvgDelaytime() << endl;
		cout << "Server Utilization " << GetServerUtilization() << endl;
		cout << "Total Service Time" << _completionTime << endl;
	}

private:
	int _eventsCount; // Maintain a count of all events in the simulation
	Time _totalFlowtime; //Maintain a sum of flow time of every entity
	Time _totalDelaytime; //Maintain a sum of  delay time of every entity
	Time _maxFlowtime; // Holds max flow time
	Time _maxDelaytime; // Holds max delay time
	Time _sumOfServiceTimes; // Hold Sum of each entities service times to help calculate server utilization
	Time _completionTime; //hold completion time of last event

};

void Sink::Depart(Entity* en)
{
	//cout << "Entity " << en->GetID() << " has departed the system." << endl;
	cout << "Entity " << en->GetID() << " arrival " << en->GetArrivalTime() <<" Start "<< en->GetServiceStartTime() << " depart "<< en->GetDepartureTime()<< endl;
	Time delayTime = en->GetServiceStartTime() - en->GetArrivalTime(),
		flowTime = en->GetDepartureTime() - en->GetArrivalTime(),
		serviceTime = en->GetDepartureTime() - en->GetServiceStartTime(),
		completionTime = en->GetDepartureTime();
	_stats.AddStats(delayTime, flowTime,serviceTime,completionTime);
}



void Sink::GetStats() {
	_stats.GetStats();
}


Sink::Stats Sink::_stats;	//instantiates the Stats object at the start of the simulation


