#pragma once
#include <string>
#include "SimObj.h"
#include "Queue.h"

class Source : public SimObj {
private:
	Queue* _queue;
	Distribution* _interarrivalTime;

	class CreateEntityEvent;
	void CreateEntity();

	string _name;
	int _numGen;

public:
	Source(string name, Queue* queue, Distribution* interarrivalTime);
	Source(string name, Queue* queue, Distribution* interarrivalTime, int numGen);
};
