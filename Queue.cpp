#include "Queue.h"
#include "SimObj.h"
#include <iostream>

using namespace std;


Queue::Queue(string name, Sink* sink, Distribution* serviceTime)
{
	_sink = sink;
	_serviceTime = serviceTime;
	_name = name;
	_prevDepartureTime = 0;
}

class Queue::ArriveEvent : public Event
{
public:
	ArriveEvent(Queue *queue, Entity *en)
	{
		_queue = queue;
		_en = en;
	}

	void Execute()
	{
		_queue->Arrive(_en);
	}

private:
	Queue *_queue;
	Entity *_en;
};

class Queue::DepartEvent : public Event {
private:
	Queue* _queue;
	Entity* _en;

public:
	DepartEvent(Queue* queue, Entity* en) {
		_queue = queue;
		_en = en;
	}
	void Execute() {
		_queue->Depart(_en);
	}
};

void Queue::ScheduleArrivalIn(Time deltaTime, Entity * en)
{
	ScheduleEventIn(deltaTime, new ArriveEvent(this, en));
}

void Queue::Arrive(Entity * en)
{
	//cout << "Entity " << en->GetID() << " has arrived at " << GetCurrentSimTime() << endl;
	Time departTime;
	if (_prevDepartureTime < GetCurrentSimTime())
	{
		departTime = GetCurrentSimTime() + _serviceTime->GetRV();
		en->SetServiceStartTime(GetCurrentSimTime());
	}
	else
	{
		departTime = _prevDepartureTime + _serviceTime->GetRV();
		en->SetServiceStartTime(_prevDepartureTime);
		
	}
	
	
	en->SetDepartureTime(departTime);
	_prevDepartureTime = departTime;
	ScheduleEventAt(departTime, new DepartEvent(this, en));
}

void Queue::Depart(Entity* en)
{
	//cout << "Entity " << en->GetID() << " has departed at " << GetCurrentSimTime() << endl;
	en->SetDepartureTime(GetCurrentSimTime());
	//_prevDepartureTime = GetCurrentSimTime();
	_sink->Depart(en); //Pass entity to sink node
}



