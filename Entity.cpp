#include "Entity.h"

int Entity::_nextID = 1;

int Entity::GetID()
{
	return _id;
}

Entity::Entity()
{
	_id = _nextID++;
	_arrivalTime = 0;
	_departureTime = 0;
	_serviceStartTime = 0;
}

Entity::Entity(Time arrivalTime)
{
	_id = _nextID++;
	_arrivalTime = arrivalTime;
	_departureTime = 0;
	_serviceStartTime = 0;
}

void Entity::SetDepartureTime(Time departureTime)
{
	_departureTime = departureTime;
}

void Entity::SetServiceStartTime(Time serviceStartTime)
{
	_serviceStartTime = serviceStartTime;
}

Time Entity::GetArrivalTime()
{
	return _arrivalTime;
}

Time Entity::GetDepartureTime()
{
	return _departureTime;
}

Time Entity::GetServiceStartTime()
{
	return _serviceStartTime;
}

