#pragma once
#include <string>
#include "Entity.h"
#include <iostream>

using namespace std;

class Sink {
private:
	string _name;
	class Stats; //container for all statistics of the simulation
	static Stats _stats; // storage object for every statics

public:
	Sink(string name);
	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	void Depart(Entity* en);

	/*
		Purpose:
		Parameters:
		Return Value:
	*/
	void GetStats();
};
