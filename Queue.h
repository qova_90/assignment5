#pragma once
#include "SimObj.h"
#include "Entity.h"
#include "Sink.h"
#include "Distribution.h"


class Queue : public SimObj
{
public:
	Queue(string name, Sink* sink, Distribution* serviceTime);
	/*
		Queue
			Parameters:
				none
			Return value:
				none
			Behavior:
				Constructor for the Queue class based on SimObj
	*/

	void ScheduleArrivalIn(Time deltaTime, Entity *en);

	private:
		Time _prevDepartureTime;
		Sink* _sink;
		string _name;
		Distribution* _serviceTime;

		//Events
		class ArriveEvent;
		class DepartEvent;

		//Event Methods
		void Arrive(Entity *en);
		void Depart(Entity* en);
	
};
