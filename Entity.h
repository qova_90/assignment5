#pragma once

typedef double Time;

class Entity
{
public:
	int GetID();
	/*
		GetID
			Paramters:
				none
			Return value:
				Entity ID number
			Behavior:
				gets the Entity unique ID number
	*/

	Entity();
	Entity(Time arrivalTime);
	void SetDepartureTime(Time departureTime);
	void SetServiceStartTime(Time serviceStartTime);
	Time GetArrivalTime();
	Time GetDepartureTime();
	Time GetServiceStartTime();

	
private:
	int _id;
	static int _nextID;
	Time _arrivalTime;
	Time _departureTime;
	Time _serviceStartTime;
	
};
