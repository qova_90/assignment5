#include "SimObj.h"
#include "Queue.h"
#include <iostream>
#include "Source.h"
#include "Sink.h"

using namespace std;

int main()
{
	Sink sink("Sink");
	Queue queue("Queue", &sink, new Triangular(2, 3, 4));
	Source source("Source", &queue, new Triangular(1, 3, 5), 100);
	SimObj::RunSimulation();
	sink.GetStats();
}